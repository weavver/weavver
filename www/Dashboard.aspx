﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Dashboard.aspx.cs" Inherits="Dashboard" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Banner" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="NavigationHolder" Runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="Content" Runat="Server">
     <div style="padding: 15px;">
          <div style="float:right;">
               <div id="AccountLinks" runat="server" visible="false" style="float:right;">
                    Accounting:
                    <a id="ReceivablesLink" runat="server" href='#'>Receivables</a> | 
                    <a id="PayablesLink" runat="server" href='#'>Payables</a>
               </div>
               <div id="SearchBox" runat="server" style="float:right; clear: right;" visible="false">
                    <asp:TextBox ID="Query" runat="server"></asp:TextBox>&nbsp;<asp:Button ID="Search" runat="server" Text="Search" OnClick="Search_Click" />
               </div>
          </div>
          <div style="width: 100%; margin-bottom: 10px; display: inline-block; max-width: 350px; text-align:left; border: 1px solid lightgrey; padding: 5px; vertical-align: top">
               <asp:ListView ID="NewsList" runat="server" AllowPaging="True" AllowSorting="True" EnableViewState="False" PageSize="1">
               <ItemTemplate>
                    <div title="<%# Eval("PublishAt") %>" style="padding-top: 5px; cursor: pointer" onclick="window.location = '<%# WeavverMaster.FormatURLs("~/") %>/Marketing_PressReleases/PressRoll.aspx'">
                         <img style="vertical-align: middle; height: 20px;" alt="" src="<%# WeavverMaster.FormatURLs("~/") %>/images/right-arrow.png" />&nbsp;&nbsp;
                         <a href="<%# WeavverMaster.FormatURLs("~/") %>Marketing_PressReleases/Details.aspx?Id=<%# Eval("Id") %>" style="color: Blue;"><%# Eval("Title") %></a>
                         <div style='text-align: right; font-style: italic; color: Gray;margin-right: 5px;'>
                              posted <%# Weavver.Utilities.DateTimeHelper.GetHumanFriendlyTimeString((DateTime.Now.Subtract((DateTime) Eval("PublishAt")).TotalSeconds)) %>
                         </div>
                    </div>
               </ItemTemplate>
               <EmptyDataTemplate>
                    <div style="clear: both;">No news is available.</div>
               </EmptyDataTemplate>
               <LayoutTemplate>
                    <div style='font-size: 14pt; border-bottom: 1px solid black;margin: 5px;'>
                         RECENT BLOGS
                    </div>
                    <div ID="itemPlaceholderContainer" runat="server" style='padding-left: 10px;'>
                         <span ID="itemPlaceholder" runat="server" />
                    </div>
                    <div style='text-align: right; margin-right: 5px;'>
                         <a href='<%# WeavverMaster.FormatURLs("~/") %>Marketing_PressReleases/PressRoll.aspx'>view all</a>
                    </div>
               </LayoutTemplate>
               </asp:ListView>
          </div>
          <asp:Literal ID="AccountContent" runat="server" />
     <%--
          <asp:UpdatePanel ID="UpdatePanel1" runat="server">
               <ContentTemplate>
                    <asp:MultiView ID="MultiView1" runat="server" ActiveViewIndex="0">
                    <asp:View ID="ApplicationsView" runat="server">
                              <br />
                              Your applications:<br />
                              <asp:ListView ID="ApplicationList" runat="server" HorizontalAlign="Center">
                              <LayoutTemplate>
                                   <table runat="server" id="table1">
                                        <tr runat="server" id="groupPlaceholder">
                                        </tr>
                                   </table>
                              </LayoutTemplate>
                              <GroupTemplate>
                                   <tr runat="server" id="tableRow">
                                        <td runat="server" id="itemPlaceholder" />
                                   </tr>
                              </GroupTemplate>
                              <ItemTemplate>
                                   <td id="Td1" runat="server">
                                        <a href="<%# Eval("AdminURL") %>"><asp:Label ID="NameLabel" runat="server" Text='<%#Eval("Name") %>' /></a>
                                   </td>
                              </ItemTemplate>
                              </asp:ListView>
                    </asp:View>
                    </asp:MultiView>
               </ContentTemplate>
               </asp:UpdatePanel>
     --%>
     <asp:LoginView ID="SiteOverview1" runat="server">
     <RoleGroups>
          <asp:RoleGroup Roles="Administrators">
          <ContentTemplate>
               <div style="width: 100%; margin-bottom: 10px; display: inline-block; max-width: 350px; text-align:left; border: 1px solid lightgrey; padding: 5px; vertical-align: top;">
                    <div style='font-size: 14pt; border-bottom: 1px solid black;margin: 5px;'>
                         Weavver Internal Control Panel
                    </div>
                    <div style='padding-left: 10px; padding-right: 20px;'>
                         <br />
                         <b>Customer Service</b><br />
                         <hr />
                         Tickets: <asp:Label ID="OpenTickets" runat="server"></asp:Label> Open, <asp:Label ID="TotalTickets" runat="server"></asp:Label> Total<br />
                         <br />
                         <asp:Panel ID="SiteOverview" runat="server" Visible="true">
                              <b>Users</b><br />
                              Sign Ups: <asp:Label ID="UsersToday" runat="server"></asp:Label> Today,
                              <asp:Label ID="UsersMonth" runat="server"></asp:Label> Per Month,
                              <asp:Label ID="UsersTotal" runat="server"></asp:Label> Total<br />
                              <br />
                              <b>Sales</b><br />
                              Orders: <asp:Label ID="OrderGross" runat="server"></asp:Label> Gross Total<br />
                         </asp:Panel>
                         <br />
                         <asp:Panel ID="Intranet" runat="server" Visible="false">
                              <br />
                              You can share this web site with others on your network by linking them to one of the following: <asp:Label ID="Self_URL" runat="server"></asp:Label><br />
                         </asp:Panel>
                    </div>
               </div>
          </ContentTemplate>
          </asp:RoleGroup>
     </RoleGroups>
     </asp:LoginView>
</div>
</asp:Content>

