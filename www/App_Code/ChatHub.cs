﻿using System;
using System.Linq;
using System.Web;
using Microsoft.AspNet.SignalR;
using System.Threading.Tasks;

public class ChatHub : Hub
{
     public override Task OnConnected()
    {
        // Add your own code here.
        // For example: in a chat application, record the association between
        // the current connection ID and user name, and mark the user as online.
        // After the code in this method completes, the client is informed that
        // the connection is established; for example, in a JavaScript client,
        // the start().done callback is executed.

          string name = Context.User.Identity.Name;
          Clients.All.debug(this.Context.ConnectionId + "@" + Context.Request.Url.DnsSafeHost + ":connected-" + name);

          using (Weavver.Data.WeavverEntityContainer db = new Weavver.Data.WeavverEntityContainer())
          {
               Weavver.Data.Vendors_Microsoft_SignalrConnections connection = new Weavver.Data.Vendors_Microsoft_SignalrConnections();
               connection.UserName = name;
               connection.ConnectedAt = DateTime.UtcNow;
               connection.Connected = true;
               connection.OrganizationId = Guid.Empty;
               connection.ConnectionId = Guid.Parse(Context.ConnectionId);
               connection.UserAgent = Context.Request.Headers["User-Agent"];
               connection.Node = Context.Request.Url.DnsSafeHost;
               db.Vendors_Microsoft_SignalrConnections.Add(connection);
               db.SaveChanges();

          }
          return base.OnConnected();
    }

     public override Task OnDisconnected(bool stopCalled)
     {
          // Add your own code here.
          // For example: in a chat application, mark the user as offline, 
          // delete the association between the current connection id and user name.
          Clients.All.debug("disconnected");

          using (Weavver.Data.WeavverEntityContainer db = new Weavver.Data.WeavverEntityContainer())
          {
               Guid cId = Guid.Parse(Context.ConnectionId);
               var connection = from c in db.Vendors_Microsoft_SignalrConnections
                                where c.ConnectionId == cId
                                select c;
               
               db.Vendors_Microsoft_SignalrConnections.RemoveRange(connection.ToList());
               db.SaveChanges();
          }
          return base.OnDisconnected(stopCalled);
     }

     public override Task OnReconnected()
     {
          // Add your own code here.
          // For example: in a chat application, you might have marked the
          // user as offline after a period of inactivity; in that case 
          // mark the user as online again.
          Clients.All.broadcastMessage("status", "reconnected");
          return base.OnReconnected();
     }

     public void Send(string name, string message)
     {
          string name2 = Context.User.Identity.Name;
          // Call the broadcastMessage method to update clients.
          Clients.All.broadcastMessage(name2, message);
     }

     public void openLogIn()
     {
          Clients.All.openLogIn();
     }
}
