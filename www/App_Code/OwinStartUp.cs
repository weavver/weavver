﻿using Microsoft.AspNet.SignalR;
using Microsoft.Owin;
using Owin;
using System.Configuration;

public class OwinStartup
{
     public void Configuration(IAppBuilder app)
     {
          string sqlConnectionString = ConfigurationManager.ConnectionStrings["signalr"].ConnectionString;
          GlobalHost.DependencyResolver.UseSqlServer(sqlConnectionString);
          app.MapSignalR();
     }
}